package ru.kopylov.tm.repository;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskOwnerRepository {

    public static Map<String, List<String>> tasksOwner = new LinkedHashMap<>();

    public void merge(String projectId, String taskId) {
        if (tasksOwner.containsKey(projectId)) tasksOwner.get(projectId).add(taskId);
        else {
            List<String> taskList = new ArrayList<>();
            taskList.add(taskId);
            tasksOwner.put(projectId, taskList);
        }
    }

    public List<String> findAllByProjectId(String projectId) {
        return tasksOwner.get(projectId);
    }

    public List<String> findAll() { //возвращает список задач, принадлежащих всем проектам
        List<String> allTaskIdList = new ArrayList<>();
        for (Map.Entry<String, List<String>> entry : tasksOwner.entrySet()) {
            allTaskIdList.addAll(entry.getValue());
        }
        return allTaskIdList;
    }

}
