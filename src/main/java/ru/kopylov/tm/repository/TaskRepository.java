package ru.kopylov.tm.repository;

import ru.kopylov.tm.entity.Task;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class TaskRepository {

    public static Map<String, Task> taskMap = new LinkedHashMap<>();

    public void merge(Task task) {
        taskMap.put(task.getId(), task);
    }

    public Task persist(Task task) {
        return taskMap.putIfAbsent(task.getId(), task);
    }

    public List<Task> findAll() {
        List<Task> tasks = new ArrayList<Task>(taskMap.values());
        return tasks;
    }

    public boolean remove(String taskName) {
        return taskMap.entrySet()
                .removeIf(entry -> entry.getValue().getName().equals(taskName));
    }

    public void removeById(String taskId) {
        taskMap.remove(taskId);
    }

    public void removeAll() {
        taskMap.clear();
    }

    public Task findOne(String taskName) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (taskName.equals(entry.getValue().getName())) {
                return entry.getValue();
            }
        }
        return null;
    }

    public List<Task> findAllById(List<String> taskIdList) {
        List<Task> taskNameList = new ArrayList<>();
        for (String taskId : taskIdList) {
            taskNameList.add(TaskRepository.taskMap.get(taskId));
        }
        return taskNameList;
    }

}
