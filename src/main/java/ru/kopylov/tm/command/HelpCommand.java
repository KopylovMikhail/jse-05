package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.util.Map;

public class HelpCommand extends AbstractCommand {

    public HelpCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public void execute() {
        for (Map.Entry<String, AbstractCommand> entry : bootstrap.getCommands().entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().getDescription());
        }
        System.out.print("\n");
    }

}
