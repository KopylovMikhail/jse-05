package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class ProjectUpdateCommand extends AbstractCommand {

    public ProjectUpdateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-update";
    }

    @Override
    public String getDescription() {
        return "Update selected project.";
    }

    @Override
    public void execute() {
        String nameOld, nameNew;
        System.out.println("[PROJECT UPDATE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            nameOld = bootstrap.getReader().readLine();
            System.out.println("ENTER NEW PROJECT NAME:");
            nameNew = bootstrap.getReader().readLine();
            if (bootstrap.getProjectService().update(nameOld, nameNew))
                System.out.println("[PROJECT " + nameOld + " UPDATED TO " + nameNew + "]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
