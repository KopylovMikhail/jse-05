package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class ProjectRemoveCommand extends AbstractCommand {

    public ProjectRemoveCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-remove";
    }

    @Override
    public String getDescription() {
        return "Remove selected project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT REMOVE]\n" +
                "ENTER EXISTING PROJECT NAME:");
        try {
            String projectName = bootstrap.getReader().readLine();
            if (bootstrap.getProjectService().remove(projectName))
                System.out.println("[PROJECT " + projectName + " REMOVED]\n");
            else System.out.println("SUCH A PROJECT DOES NOT EXIST OR NAME IS EMPTY.\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
