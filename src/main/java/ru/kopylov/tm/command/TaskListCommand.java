package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.util.List;

public class TaskListCommand extends AbstractCommand {

    public TaskListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-list";
    }

    @Override
    public String getDescription() {
        return "Show all tasks.";
    }

    @Override
    public void execute() {
        List<String> taskList = bootstrap.getTaskService().list();
        System.out.println("[TASK LIST]");
        int i = 1;
        for (String task : taskList) {
            System.out.println(i++ + ". " + task);
        }
        System.out.print("\n");
    }

}
