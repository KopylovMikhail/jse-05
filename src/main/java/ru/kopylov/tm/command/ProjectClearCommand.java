package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

public class ProjectClearCommand extends AbstractCommand {

    public ProjectClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public void execute() {
        bootstrap.getProjectService().clear();
        System.out.println("[ALL PROJECTS REMOVED]\n");
    }

}
