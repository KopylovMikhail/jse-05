package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.util.List;

public class ProjectListCommand extends AbstractCommand {

    public ProjectListCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "project-list";
    }

    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() {
        List<String> projectList = bootstrap.getProjectService().list();
        System.out.println("[PROJECT LIST]");
        int i = 1;
        for (String project : projectList) {
            System.out.println(i++ + ". " + project);
        }
        System.out.print("\n");
    }

}
