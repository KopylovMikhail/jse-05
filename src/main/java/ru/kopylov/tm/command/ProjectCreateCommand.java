package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class ProjectCreateCommand extends AbstractCommand {

    public ProjectCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return  "project-create";
    }

    @Override
    public String getDescription() {
        return "Create new project.";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]\n" +
                "ENTER NAME:");
        try {
            String projectName = bootstrap.getReader().readLine();
            if (bootstrap.getProjectService().create(projectName))
                System.out.println("[OK]\n");
            else System.out.println("[SUCH A PROJECT EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
