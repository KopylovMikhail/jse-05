package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

public class TaskClearCommand extends AbstractCommand {

    public TaskClearCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public String getDescription() {
        return "Remove all tasks.";
    }

    @Override
    public void execute() {
        bootstrap.getTaskService().clear();
        System.out.println("[ALL TASKS REMOVED]\n");
    }

}
