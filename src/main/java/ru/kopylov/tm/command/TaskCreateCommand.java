package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

import java.io.IOException;

public class TaskCreateCommand extends AbstractCommand {

    public TaskCreateCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "task-create";
    }

    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() {
        System.out.println("[TASK CREATE]\n" +
                "ENTER NAME:");
        try {
            String taskName = bootstrap.getReader().readLine();
            if (bootstrap.getTaskService().create(taskName)) System.out.println("[OK]\n");
            else System.out.println("[SUCH A TASK EXISTS OR NAME IS EMPTY.]\n");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
