package ru.kopylov.tm.command;

import ru.kopylov.tm.context.Bootstrap;

public class ExitCommand extends AbstractCommand {

    public ExitCommand(Bootstrap bootstrap) {
        super(bootstrap);
    }

    @Override
    public String getName() {
        return "exit";
    }

    @Override
    public String getDescription() {
        return "Exit from application.";
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
