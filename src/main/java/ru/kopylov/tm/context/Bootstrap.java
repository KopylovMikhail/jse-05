package ru.kopylov.tm.context;

import ru.kopylov.tm.command.*;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;
import ru.kopylov.tm.service.ProjectService;
import ru.kopylov.tm.service.TaskService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;

public class Bootstrap {

    private BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    private Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    private ProjectRepository projectRepository = new ProjectRepository();

    private TaskRepository taskRepository = new TaskRepository();

    private TaskOwnerRepository taskOwnerRepository = new TaskOwnerRepository();

    private ProjectService projectService = new ProjectService(projectRepository, taskRepository, taskOwnerRepository);

    private TaskService taskService = new TaskService(taskRepository);

    public ProjectService getProjectService() {
        return projectService;
    }

    public TaskService getTaskService() {
        return taskService;
    }

    public BufferedReader getReader() {
        return reader;
    }

    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    private void registry(AbstractCommand command) {
        commands.put(command.getName(), command);
    }

    public void init() {
        {
            registry(new HelpCommand(this));
            registry(new ProjectCreateCommand(this));
            registry(new ProjectListCommand(this));
            registry(new ProjectUpdateCommand(this));
            registry(new ProjectRemoveCommand(this));
            registry(new ProjectClearCommand(this));
            registry(new ProjectSetTaskCommand(this));
            registry(new ProjectTasksListCommand(this));
            registry(new TaskCreateCommand(this));
            registry(new TaskListCommand(this));
            registry(new TaskUpdateCommand(this));
            registry(new TaskRemoveCommand(this));
            registry(new TaskClearCommand(this));
            registry(new ExitCommand(this));
        }
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        String command = "";
        do {
            try {
                command = reader.readLine();
                execute(command);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } while (!"exit".equals(command));
    }

    private void execute(String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        abstractCommand.execute();
    }

}
