package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class TaskService {

    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public boolean create(String taskName) {
        if (taskName == null || taskName.isEmpty()) return false;
        Task task = new Task();
        task.setName(taskName);
        return !task.equals(taskRepository.persist(task));
    }

    public List<String> list() {
        List<String> taskNameList = new ArrayList<>();
        for (Task task : taskRepository.findAll()) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

    public boolean update(String nameOld, String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (taskRepository.findOne(nameOld) == null) return false;
        Task task = taskRepository.findOne(nameOld);
        task.setName(nameNew);
        taskRepository.merge(task);
        return true;
    }

    public boolean remove(String taskName) {
        if (taskName == null || taskName.isEmpty()) return false;
        return taskRepository.remove(taskName);
    }

    public void clear() {
        taskRepository.removeAll();
    }

}
