package ru.kopylov.tm.service;

import ru.kopylov.tm.entity.Project;
import ru.kopylov.tm.entity.Task;
import ru.kopylov.tm.repository.ProjectRepository;
import ru.kopylov.tm.repository.TaskOwnerRepository;
import ru.kopylov.tm.repository.TaskRepository;

import java.util.ArrayList;
import java.util.List;

public class ProjectService {

    private ProjectRepository projectRepository;

    private TaskRepository taskRepository;

    private TaskOwnerRepository taskOwnerRepository;

    public ProjectService(ProjectRepository projectRepository,
                          TaskRepository taskRepository,
                          TaskOwnerRepository taskOwnerRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
        this.taskOwnerRepository = taskOwnerRepository;
    }

    public boolean create(String projectName) {
        if (projectName == null || projectName.isEmpty()) return false;
        Project project = new Project();
        project.setName(projectName);
        return !project.equals(projectRepository.persist(project));
    }

    public List<String> list() {
        List<String> projectNameList = new ArrayList<>();
        for (Project project : projectRepository.findAll()) {
            projectNameList.add(project.getName());
        }
        return projectNameList;
    }

    public boolean update(String nameOld, String nameNew) {
        if (nameOld == null || nameOld.isEmpty()) return false;
        if (nameNew == null || nameNew.isEmpty()) return false;
        if (projectRepository.findOne(nameOld) == null) return false;
        Project project = projectRepository.findOne(nameOld);
        project.setName(nameNew);
        projectRepository.merge(project);
        return true;
    }

    public boolean remove(String projectName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        String projectId = projectRepository.findOne(projectName).getId();
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        for (String taskId : taskIdList) {
            taskRepository.removeById(taskId); //вместе с удалением проекта удаляем все задачи проекта
        }
        return projectRepository.remove(projectName);
    }

    public void clear() {
        List<String> allTaskIdList = taskOwnerRepository.findAll();
        for (String taskId : allTaskIdList) {
            taskRepository.removeById(taskId); //удаляя все проекты, удаляем все задачи проектов
        }
        projectRepository.removeAll();
    }

    public boolean setTask(String projectName, String taskName) {
        if (projectName == null || projectName.isEmpty()) return false;
        if (taskName == null || taskName.isEmpty()) return false;
        if (projectRepository.findOne(projectName) == null) return false;
        if (taskRepository.findOne(taskName) == null) return false;
        String projectId = projectRepository.findOne(projectName).getId();
        String taskId = taskRepository.findOne(taskName).getId();
        taskOwnerRepository.merge(projectId, taskId);
        return true;
    }

    public List<String> tasksList(String projectName) { //возвращает список задач проекта
        if (projectName == null || projectName.isEmpty()) return null;
        if (projectRepository.findOne(projectName) == null) return null;
        String projectId = projectRepository.findOne(projectName).getId();
        if (taskOwnerRepository.findAllByProjectId(projectId) == null) return null;
        List<String> taskIdList = taskOwnerRepository.findAllByProjectId(projectId);
        if (taskRepository.findAllById(taskIdList) == null) return null;
        List<Task> taskList = taskRepository.findAllById(taskIdList);
        List<String> taskNameList = new ArrayList<>();
        for (Task task : taskList) {
            taskNameList.add(task.getName());
        }
        return taskNameList;
    }

}
